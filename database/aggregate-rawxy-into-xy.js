var mongo =  require('./lib/mongo');

mongo.then(function(db) {
	var collection = db.collection('rawxy');

	collection.aggregate([
		{ $group: {
			_id: {
				timeSlot: { $divide: [ { $subtract: [ '$time', { $mod: [ '$time', 5000 ] } ] }, 5000 ] },
				matchId: '$matchId',
				team: '$team',
				player: '$player'
			},
			tier: { $first: '$tier' },
			winner: { $first: '$winner' },
			x: { $first: '$x' },
			y: { $first: '$y' }
		} },
		{ $group: {
			_id: {
				timeSlot: '$_id.timeSlot',
				matchId: '$_id.matchId',
				team: '$_id.team'
			},
			tier: { $first: '$tier' },
			winner: { $first: '$winner' },
			players: { $push: {
				player: '$_id.player',
				x: '$x',
				y: '$y'
			} }
		} },
		{ $group: {
			_id: {
				timeSlot: '$_id.timeSlot',
				matchId: '$_id.matchId'
			},
			tier: { $first: '$tier' },
			winner: { $first: '$winner' },
			teams: { $push: {
				team: '$_id.team',
				players: '$players'
			} }
		} },
		{ $out: 'xy' }
	], {
		allowDiskUse: true
	}, function(error, entries) {
		if (error) { return console.error(error); }
		console.log(entries.length);
		db.close();
	});
})
