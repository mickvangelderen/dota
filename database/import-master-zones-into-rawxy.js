var CsvParser = require('./lib/csv-parser');
var fs = require('fs');
var Grouper = require('./lib/grouper');
var LineParser = require('./lib/line-parser');
var MapStream = require('./lib/map-stream');
var mongo =  require('./lib/mongo');
var Promise = require('es6-promise').Promise;

var filePath = 'master-zones.csv';
var collectionName = 'rawxy';
var database = null;
var collection = null;

mongo.then(function(_database) {

	database = _database;
	collection = database.collection(collectionName);

	return new Promise(function(resolve, reject) {

		var startDate = new Date();
		var size = fs.statSync(filePath).size;
		var processed = 0;
		var percent = 0;
		var inserted = 0;
		var insertedNotify = 500000;

		console.log(startDate.toISOString(), 'Inserting documents into', collectionName + 'from', filePath + '...');

		var stream = fs.createReadStream(filePath)
			.on('data', function(chunk) {
				processed += chunk.length;
				var newPercent = Math.round(processed/size*100);
				if (newPercent != percent) {
					percent = newPercent;
					console.log(new Date().toISOString(), percent + '%');
				}
			})
			.pipe(new LineParser())
			.pipe(new CsvParser())
			.pipe(new MapStream(function(d) {
				var t = d.team;
				var w = d.won === 1;
				return {
					matchId: d.match,
					tier: d.tier.toLowerCase(),
					winner: (t === 'radiant' && w) || (t === 'dire' && !w) ? 'radiant' : 'dire',
					time: +d.tstd,
					team: t,
					player: d.player,
					x: +d.x,
					y: +d.y
				};
			}))
			.pipe(new Grouper(1000));

		stream
			.on('data', function(entries) {
				inserted += entries.length;
				stream.pause();
				collection.insert(entries, {
					w: 1,
					ordered: false
				}, function(error, result) {
					if (error) return reject(error);
					inserted += entries.length;
					if (inserted >= insertedNotify) {
						insertedNotify += 500000;
						console.log(new Date().toISOString(), 'Inserted', inserted, 'documents.');
					}
					stream.resume();
				});
			})
			.on('end', function() {
				var endDate = new Date();
				console.log(endDate.toISOString(), 'Inserted', inserted, 'documents.');
				console.log(endDate.toISOString(), 'Took', Math.round((endDate - startDate)/1000), 'seconds.');
				resolve();
			});
	});

}).then(function() {

	return new Promise(function(resolve, reject) {
		var startDate = new Date();
		console.log(startDate.toISOString(), 'Building index on matchId...');
		collection.ensureIndex({ matchId: 1 }, function(error, result) {
			if (error) return reject(error);
			var endDate = new Date();
			console.log(endDate.toISOString(), 'Index on matchId built.');
			console.log(endDate.toISOString(), 'Took', Math.round((endDate - startDate)/1000), 'seconds.');
			resolve();
		});

	});

}).then(function() {
	console.log(new Date().toISOString(), 'Done.')
}, function(error) {
	console.error(error);
}).then(function() {
	if (database) database.close();
});