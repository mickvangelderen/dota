var mongo =  require('./lib/mongo');
var sequentialPromise = require('./lib/sequential-promise');
var Promise = require('es6-promise').Promise;

// grab all matches ASYNC

// each match SEQUENTIAL

//	grab all timeslots for match ordered low to high ASYNC

//		each timeslot SEQUENTIAL

//			add player to player list if its not yet there

// 		for 0 to max(timeslots in match) SEQUENTIAL

//			get / create timeslot ASYNC

// 			update position

// 			compute distance

// 			save ASYNC



var inCollectionName = 'xy';
var outCollectionName = 'd';
var database = null;
var collection = null;

mongo.then(function(_database) {

	database = _database;

	collection = database.collection(inCollectionName);

	return getAllMatchIds(database);

}, function(error) {
	console.error(error, error.stack);
}).then(function(matchIds) {
	return sequentialPromise(matchIds, processMatch);
}).then(function(results) {
	console.log(results.length);
	database.close();
}).catch(function(error) {
	console.error(error, error.stack);
	database.close();
});

function getAllMatchIds(database) {
	return new Promise(function(resolve, reject) {
		database.collection(inCollectionName)
			.distinct('_id.matchId', function(error, entries) {
				if (error) return reject(error);
				resolve(entries);
			});
	});
}

function getAllTimeSlotsForMatch(database, matchId) {
	return new Promise(function(resolve, reject) {
		database.collection(inCollectionName)
			.find({ '_id.matchId': matchId })
			.sort({ '_id.timeSlot': 1 })
			.toArray(function(error, entries) {
				if (error) return reject(error);
				resolve(entries);
			});
	});
}

function find(list, query) {
	for (var i = 0, l = list.length >>> 0; i < l; i++) {
		var value = list[i];
		if (query(value)) return value;
	}
	return undefined;
}

function processMatch(matchId) {
	// compute starting position
	console.log("Processing match", matchId);

	return getAllTimeSlotsForMatch(database, matchId)
	.then(function(entries) {

		// compute player list
		var playerDataByPlayerName = {};
		var playerDataList = [];
		var i, l;
		for (i = 0, l = entries.length; i < l; i++) {
			var entry = entries[i];
			entry.teams.forEach(function(team) {
				team.players.forEach(function(player) {
					var playerData = playerDataByPlayerName[player.player];
					if (playerData) return;
					playerData = {
						player: player.player,
						team: team.team,
						x: player.x,
						y: player.y
					};
					playerDataByPlayerName[playerData.player] = playerData;
					playerDataList.push(playerData);
				});
			});
			if (playerDataList.length === 10) break;
		}

		// compute new entries
		var newEntries = [];
		var timeSlot = 0;

		for (i = 0, l = entries.length; i < l; i++) {
			var entry = entries[i];

			// fill empty timeslots
			while (timeSlot < entry._id.timeSlot) {
				console.log('Filling empty timeslot for match', matchId, 'time slot', timeSlot);
				var data = {
					_id: {
						matchId: entry._id.matchId,
						timeSlot: timeSlot
					},
					tier: entry.tier,
					winner: entry.winner,
					teams: []
				};
				playerDataList.forEach(function(playerData) {
					var team = find(data.teams, function(entry) {
						return entry.team === playerData.team;
					});
					if (!team) {
						team = { team: playerData.team, players: [] };
						data.teams.push(team);
					}
					team.players.push({
						player: playerData.player,
						x: playerData.x,
						y: playerData.y
					});
				});

				newEntries.push(data);
				timeSlot++;
			}

			// extend existing timeslots
			playerDataList.forEach(function(playerData) {
				var team = find(entry.teams, function(entry) {
					return entry.team === playerData.team;
				});
				if (!team) {
					team = { team: playerData.team, players: [] };
					entry.teams.push(team);
				}
				var player = find(team.players, function(entry) {
					return entry.player === playerData.player;
				});
				if (player) {
					// player exists, update playerData position
					playerData.x = player.x;
					playerData.y = player.y;
				} else {
					// player does not exist, use previous position
					team.players.push({
						player: playerData.player,
						x: playerData.x,
						y: playerData.y
					});
				}
			});

			newEntries.push(entry);
			timeSlot = entry._id.timeSlot + 1;

		} // end of for loop

		newEntries.forEach(function(entry) {
			// compute distance
			entry.teams.forEach(function(team) {
				team.players.forEach(function(player) {
					var distance = -1;
					team.players.forEach(function(other) {
						var dx = player.x - other.x;
						var dy = player.y - other.y;
						if (dx*dx + dy*dy < 20*20) distance++;
					});
					player.d = distance;
				});
			});
		});

		return sequentialPromise(newEntries, saveTimeSlot);
	});
}

function saveTimeSlot(entry) {
	return new Promise(function(resolve, reject) {
		database.collection(outCollectionName).insert(entry, function(error, result) {
			if (error) return reject(error);
			resolve(result);
		});
	});
}