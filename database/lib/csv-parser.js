var inherits = require('util').inherits;
var Transform = require('stream').Transform;

inherits(CsvParser, Transform);

module.exports = CsvParser;

function CsvParser(options) {
	if (!(this instanceof CsvParser)) { return new CsvParser(options); }

	Transform.call(this, options);

	this._writableState.objectMode = true;
	this._readableState.objectMode = true;
	this._headers = null;
}

CsvParser.prototype._transform = function(line, encoding, done) {
	if (line.length === 0) { return done(); }

	var regex = /(?:^|,)(?:"((?:""|[^"])*)"|([^,]*))/g;
	var match = null;
	var values = [];
	while (match = regex.exec(line)) {
		values.push(match[1] !== undefined ? match[1].replace('""', '"') : match[2]);
	}

	if (this._headers === null) {
		this._headers = values;
		return done();
	}

	var object = Object.create(null);
	this._headers.forEach(function(header, index) {
		object[header] = values[index];
	});
	this.push(object);
	done();
};