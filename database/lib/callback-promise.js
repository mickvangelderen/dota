var Promise = require('es6-promise').Promise;

module.exports = callbackPromise;

function callbackPromise(creator) {
	return new Promise(function(resolve, reject) {
		creator(function(error, result) {
			if (error) return reject(error);
			resolve(result);
		});
	});
}