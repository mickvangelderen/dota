var inherits = require('util').inherits;
var StringDecoder = require('string_decoder').StringDecoder;
var Transform = require('stream').Transform;

inherits(LineParser, Transform);

module.exports = LineParser;

function LineParser(options) {
	if (!(this instanceof LineParser)) { return new LineParser(options); }

	Transform.call(this, options);

	this._writableState.objectMode = false;
	this._readableState.objectMode = true;
	this._decoder = new StringDecoder('utf8');
	this._remainder = '';
}

LineParser.prototype._transform = function(chunk, encoding, done) {
	// Prepend input to the decoded string
	var input = this._remainder + this._decoder.write(chunk);
	// Split the input per line
	var lines = input.split(/\r?\n/);
	// Last element is an unfinished line, save it in the remainder
	this._remainder = lines.pop();
	// Push all lines onto the stream
	for (var i = 0, l = lines.length; i < l; i++) { this.push(lines[i]); }
	done();
};

LineParser.prototype._flush = function(done) {
	this.push(this._remainder);
	done();
};