var MongoClient = require('mongodb').MongoClient;
var Promise = require('es6-promise').Promise;

module.exports = new Promise(function(resolve, reject) {
	MongoClient.connect('mongodb://localhost:27017/dota', function(error, database) {
		if (error) { return reject(error); }
		resolve(database);
	});
});