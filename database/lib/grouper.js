var inherits = require('util').inherits;
var Transform = require('stream').Transform;

inherits(Grouper, Transform);

module.exports = Grouper;

function Grouper(size, options) {
	if (!(this instanceof Grouper)) { return new Grouper(options); }

	Transform.call(this, options);

	this._writableState.objectMode = true;
	this._readableState.objectMode = true;
	this._size = size;
	this._accumulator = [];
}

Grouper.prototype._transform = function(entry, encoding, done) {
	this._accumulator.push(entry);
	if (this._accumulator.length >= this._size) {
		this.push(this._accumulator);
		this._accumulator = [];
	}
	done();
};

Grouper.prototype._flush = function(done) {
	this.push(this._accumulator);
	this._accumulator = [];
	done();
};