var inherits = require('util').inherits;
var Transform = require('stream').Transform;

inherits(MapStream, Transform);

module.exports = MapStream;

// Does not preserve order
function MapStream(map, options) {
	if (!(this instanceof MapStream)) { return new MapStream(map, options); }

	Transform.call(this, options);

	this._writableState.objectMode = true;
	this._readableState.objectMode = true;
	this.map = map;
}

MapStream.prototype._transform = function(from, encoding, done) {
	var to = this.map(from);
	if (to && typeof to.then === 'function') {
		to.then(function(to) {
			this.push(to); done();
		}, function(error) {
			this.push(error); done();
		});
	} else {
		this.push(to); done();
	}
};
