var Promise = require('es6-promise').Promise;

module.exports = sequentialPromise;

function sequentialPromise(array, promisor) {
	var results = [];
	var index = 0;

	return new Promise(function(resolve, reject) {

		function next() {
			if (index === array.length) return resolve(results);

			promisor(array[index]).then(onSuccess(index++), reject);
		}

		function onSuccess(i) {
			return function(result) {
				results[i] = result;
				next();
			};
		}

		next();
	});
}
