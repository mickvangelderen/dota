var mongo =  require('./lib/mongo');
var callbackPromise = require('./lib/callback-promise');

var database = null;

mongo.then(function(_database) {

	database = _database;

		console.log('Aggregating data from d into m...');
	return callbackPromise(function(callback) {
		database.collection('d').aggregate([
			{ $unwind: '$teams' },
			{ $unwind: '$teams.players' },
			{ $group: {
				_id: {
					timeSlot: '$_id.timeSlot',
					team: '$teams.team',
					tier: '$tier',
					x: '$teams.players.x',
					y: '$teams.players.y'
				},
				d: { $sum: '$teams.players.d' }
			} },
			{ $out: 'm' }
		], {
			allowDiskUse: true,
		}, callback);
	}).then(function(result) { console.log(result); return result; });

}).then(function() {

	return callbackPromise(function(callback) {
		console.log('Creating index on _id.timeSlot...');
		database.collection('m').ensureIndex({ '_id.timeSlot': 1 }, callback);
	}).then(function(result) { console.log(result); return result; });

}).then(function() {

	return callbackPromise(function(callback) {
		console.log('Creating index on _id.team...');
		database.collection('m').ensureIndex({ '_id.team': 1 }, callback);
	}).then(function(result) { console.log(result); return result; });

}).then(function() {

	return callbackPromise(function(callback) {
		console.log('Creating index on _id.tier...');
		database.collection('m').ensureIndex({ '_id.tier': 1 }, callback);
	}).then(function(result) { console.log(result); return result; });

}).catch(function(error) {
	console.error(error);
}).then(function() {
	if (database) database.close();
});