# Getting ready

Download the master-zones.csv file distributed by the Data Visualization course manager. 

```bash
git clone <url> dota
cd dota
npm install
bower install
```

Download the mongodb zip for your os and extract it somewhere. Add the mongodb/bin folder to your path. 

Start a mongo deamon:
```bash
chmod +x dota/launch-mongo.sh # first time, make it executable
dota/launch-mongo
```

Run the following commands ony by one, each of them might take a while. The database that mongodb creates might become around 10Gb. 
```bash
# 1 import the csv into mongodb
cd ~/folder-with-master-zones.csv/
node path-to-dota/database/import-master-zones-into-rawxy.js

# 2 create timeslots with nested team and player objects
cd path-to-dota/
node database/aggregate-rawxy-into-xy.js

# 3 compute d's
node database/compute-distances-from-xy-into-d.js

# 2 aggregate again
node database/aggregate-d-into-m.js
```

Start the server:
```bash
node dota/index.js
```

# Idea

Our idea was to try and find locations and the time at which team members gather to secure important objectives. 

To get a sense of when team members are gathering we grouped the player position data into timeslots of 5 seconds. We saved the first position of a player encountered in each time slot. 

We looked for the first known position of a player in each match and extended it backwards to time slot 0. 

Then we filled all time slot gaps for all matches for all players by extending their last known location forward. This means that if a player dies somewhere the x and y will not change nor disappear from the visualization. 

With the x and y positions we calculated a metric we call ‘d’. Its the number of allies in a 20 map units radius. The map size is 123x123 map units. 

This data was aggregated into a more compact { time slot, tier, team, x, y } -> d format. 

Querying and aggregating using mongodb we could easily obtain the d values for all x and y for a certain time slot, tier and team. 

# Challenges

What we would have liked to see is not yet clearly visible from the visualization we currently have. Somehow making grouping efforts stand out even more might improve this. Or maybe the entire approach is flawed with this amount of data or distance metric (d). 