#!/bin/bash

type mongod >/dev/null 2>&1 || { echo >&2 "Error: mongod command not found, download mongodb and add the bin folder to your path. "; exit 1; }

mongod --dbpath `dirname $0`/database/data