/* slider */

var slider = d3.slider()
	.axis(true)
	.margin(20)
	.min(0)
	.max(900)
	.step(1)
	.on("slide", function(evt, value) {
		setTimeSlot(value);
	})

d3.select('#timeslot-slider').call(slider);

/* rendering */

var renderOptions = {
	timeSlot: 0
};

function setTimeSlot(slot) {
	if (renderOptions.timeSlot === slot || slot > 900) return;
	renderOptions.timeSlot = slot;
	slider.value(slot);
	render(renderOptions);
}

function setStyle(style) {
	if (renderOptions.style === style) return;
	renderOptions.style = style;
	render(renderOptions);
}

var proGraph = new ezgraph.MCircles('#pro-graph', {
	width: 400,
	height: 400
});

var casualGraph = new ezgraph.MCircles('#casual-graph', {
	width: 400,
	height: 400
});

var requests = [];

function render(options) {

	requests.forEach(function(request) { if (request) request.abort(); });

	requests = [
		d3.json('/api/m/timeslot/' + options.timeSlot + '?tier=pro&team=radiant', function(error, data) {
			if (error) return console.error(error);
			requests[0] = null;
			proGraph.render(data, { team: 'radiant', style: options.style });
		}),
		d3.json('/api/m/timeslot/' + options.timeSlot + '?tier=pro&team=dire', function(error, data) {
			if (error) return console.error(error);
			requests[1] = null;
			proGraph.render(data, { team: 'dire', style: options.style });
		}),
		d3.json('/api/m/timeslot/' + options.timeSlot + '?tier=normal&team=radiant', function(error, data) {
			if (error) return console.error(error);
			requests[2] = null;
			casualGraph.render(data, { team: 'radiant', style: options.style });
		}),
		d3.json('/api/m/timeslot/' + options.timeSlot + '?tier=normal&team=dire', function(error, data) {
			if (error) return console.error(error);
			requests[3] = null;
			casualGraph.render(data, { team: 'dire', style: options.style });
		})
	];

}

d3.select('select#style-dropdown')
	.on('change', function() {
		var select = d3.select(this);
		var i = select.property('selectedIndex');
		var options = select.selectAll('option');
		setStyle(options[0][i].__data__);
	})
	.selectAll('option').data(['simple', 'size', 'opacity'])
		.enter()
			.append('option')
				.text(function(d) { return d; })

setTimeSlot(0);

setTimeout(function re() {
	if (requests.filter(function(r) { return r; }).length === 0) {
		setTimeSlot(renderOptions.timeSlot + 1);
	}
	setTimeout(re, 200);
}, 0);