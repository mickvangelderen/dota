var renderOptions = {
	matchId: null,
	timeSlot: 0
};

var matchData = null;
var matchesList = null;
var matchesDropdown = null;
var matchesOptions = null;

function setMatchId(id) {
	renderOptions.matchId = id;
	matchData = null;
	setTimeSlot(0);
}

/* matches */

function onMatchesDropdownChange() {
	var selectedIndex = matchesDropdown.property('selectedIndex');
	var data = matchesOptions[0][selectedIndex].__data__;
	setMatchId(data);
}

console.log("Querying matches...");
d3.json("/api/matches", function(error, matches) {
	if (error) return console.error(error);
	console.log("Obtained matches", matches);

	matchesList = matches;

	matchesDropdown = d3.select("select#matches-dropdown").on("change", onMatchesDropdownChange);

	matchesOptions = matchesDropdown.selectAll('option').data(matchesList);

	// update number of option elements
	matchesOptions.exit().remove();
	matchesOptions.enter().append("option");

	// update all option element attributes
	matchesOptions.text(function(d) { return d; });

	if (!renderOptions.matchId) {
		setMatchId(matchesList[0]);
	}
});


/* slider */

var slider = d3.slider()
	.axis(true)
	.margin(20)
	.min(0)
	.max(900)
	.step(1)
	.on("slide", function(evt, value) {
		setTimeSlot(value);
	})

d3.select('#timeslot-slider').call(slider);

/* rendering */

function setTimeSlot(slot) {
	if (renderOptions.timeSlot === slot || slot > 900) return;
	renderOptions.timeSlot = slot;
	slider.value(slot);
	render(renderOptions);
}

function queryMatchData(options) {
	console.log("Querying match data...", options);
	d3.json("/api/d/match/" + options.matchId, function(error, data) {
		if (error) return console.error(error);
		console.log("Obtained match data", options, data);
		matchData = data;

		render(options);
	});
}

var margin = {top: 20, right: 80, bottom: 30, left: 50},
	width = 500 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

var scales = {
	x: d3.scale.linear().domain([0, 123]).range([0, width]),
	y: d3.scale.linear().domain([0, 123]).range([height, 0]),
	team: d3.scale.category10()
};

var svg = d3.select("#heatmap").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.style({ 'display': 'block', 'margin': '0 auto' })
	.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

function render(options) {
	if (!matchData) return queryMatchData(options);

	console.log("Rendering with options", options);

	var entries = [];

	d3.select('#tier').text(matchData[0].tier);
	d3.select('#winner').text(matchData[0].winner);

	matchData.forEach(function(datum) {
		if (datum._id.timeSlot !== options.timeSlot) return;
		datum.teams.forEach(function(team) {
			team.players.forEach(function(player) {
				entries.push({
					team: team.team,
					player: player.player,
					x: player.x,
					y: player.y,
					d: player.d
				});
			});
		});
	});

    var circles = svg.selectAll("circle")
    	.data(entries, function(d) { return d.player; });

    circles.exit().remove();
    circles.enter().append("circle")
        .style("fill", function(d) { return scales.team(d.team); });

    circles
		.transition(250)
    	.attr("cx", function(d) { return scales.x(d.x); })
    	.attr("cy", function(d) { return scales.y(d.y); })
    	.attr("r", function(d) { return d.d + 1; })

    console.log(circles, entries);

}

setTimeSlot(0);

setTimeout(function re() {
	setTimeSlot(renderOptions.timeSlot + 1);
	setTimeout(re, 500);
}, 500);