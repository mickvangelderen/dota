!function(root, factory) {
	root.ezgraph = factory();
}(this, function() {

	function MCircles(selector, options) {
		if (!(this instanceof MCircles)) return new MCircles(element, options);

		this.outer = d3.select(selector).append('svg');
		this.inner = this.outer.append('g');

		this.resize(options);
	}

	MCircles.prototype.resize = function(options) {
		// required
		var w = options.width;
		var h = options.height;
		// optional
		var m = options.margin || { top: 0, right: 0, bottom: 0, left: 0 };
		// computed
		var t = m.top || 0;
		var r = m.right || 0;
		var b = m.bottom || 0;
		var l = m.left || 0;
		var iw = w - l - r;
		var ih = h - t - b;

		this.scales = {
			x: d3.scale.linear().domain([0, 123]).range([0, iw]),
			y: d3.scale.linear().domain([0, 123]).range([ih, 0]),
			r: d3.scale.linear().domain([0, 4]).range([0, 4]),
			opacity: d3.scale.linear().domain([0, 30]).range([0, 1]),
			team: d3.scale.category10()
		};

		this.outer
			.attr('width', w)
			.attr('height', h)
			.style({ 'display': 'block', 'margin': '0 auto' });

		this.inner
			.attr('transform', 'translate(' + l + ',' + t + ')');
	}

	MCircles.prototype.render = function(entries, options) {
		var scales = this.scales;

		// attach new entries to current circles
		var circles = this.inner.selectAll('circle.' + options.team).data(entries);

		// remove unneccesary circles
		circles.exit().remove();

		// append new circles
		circles.enter().append('circle')
			.classed(options.team, true)
			.style('fill', scales.team(options.team));

		// set circle properties
		circles
			.attr('cx', function(d) { return scales.x(d._id.x); })
			.attr('cy', function(d) { return scales.y(d._id.y); })
		if (options.style == 'size') {
			circles
				.attr('r', function(d) { return scales.r(d.d); })
				.style('opacity', 1);
		} else if (options.style == 'opacity') {
			circles
				.attr('r', 10)
				.style('opacity', function(d) { return scales.opacity(d.d); });
		} else {
			circles
				.attr('r', 1)
				.style('opacity', 1);
		}
	};

	return {
		MCircles: MCircles
	};
})
