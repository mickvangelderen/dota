var Router = require('express').Router;

var router = module.exports = new Router();

router.get('/', function(req, res) {
  res.render('index');
});

router.get('/match-explorer', function(req, res) {
  res.render('match-explorer');
});