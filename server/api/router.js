var Router = require('express').Router;
var mongo = require('./mongo');
var router = module.exports = new Router();

router.get('/matches', function(req, res) {
	mongo.then(function(db) {

		db.collection('xy').distinct('_id.matchId', function(error, entries) {
			if (error) return res.status(500).json(error);
			res.json(entries);
		});

	}).catch(function(error) {
		res.status(500).json(error);
	});
});

router.get('/timeslots', function(req, res) {
	mongo.then(function(db) {

		db.collection('xy').distinct('_id.timeSlot', function(error, entries) {
			if (error) return res.status(500).json(error);
			res.json(entries);
		});

	}).catch(function(error) {
		res.status(500).json(error);
	});
});

router.get('/xy/match/:id', function(req, res) {
	mongo.then(function(db) {

		db.collection('xy').find({
			'_id.matchId': req.param('id')
		}).toArray(function(error, entries) {
			if (error) return res.status(500).json(error);
			res.json(entries);
		});

	}).catch(function(error) {
		res.status(500).json(error);
	});
});

router.get('/xy/timeslot/:id', function(req, res) {
	mongo.then(function(db) {

		db.collection('xy').find({
			'_id.timeSlot': +req.param('id')
		}).toArray(function(error, entries) {
			if (error) return res.status(500).json(error);
			res.json(entries);
		});

	}).catch(function(error) {
		res.status(500).json(error);
	});
});

router.get('/d/match/:id', function(req, res) {
	mongo.then(function(db) {

		db.collection('d').find({
			'_id.matchId': req.param('id')
		}).toArray(function(error, entries) {
			if (error) return res.status(500).json(error);
			res.json(entries);
		});

	}).catch(function(error) {
		res.status(500).json(error);
	});
});

router.get('/d/timeslot/:id', function(req, res) {
	mongo.then(function(db) {

		db.collection('d').find({
			'_id.timeSlot': +req.param('id')
		}).toArray(function(error, entries) {
			if (error) return res.status(500).json(error);
			res.json(entries);
		});

	}).catch(function(error) {
		res.status(500).json(error);
	});
});

router.get('/m/timeslot/:id', function(req, res) {
	mongo.then(function(db) {

		var query = {
			'_id.timeSlot': +req.param('id')
		};
		if (req.param('tier')) query['_id.tier'] = req.param('tier');
		if (req.param('team')) query['_id.team'] = req.param('team');

		db.collection('m').aggregate([
			{ $match: query },
			{ $group: {
				_id: {
					timeSlot: '$_id.timeSlot',
					x: '$_id.x',
					y: '$_id.y'
				},
				d: { $sum: '$d' }
			} }
		], function(error, entries) {
			if (error) return res.status(500).json(error);
			res.json(entries);
		});

	}).catch(function(error) {
		res.status(500).json(error);
	});
});