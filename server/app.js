var express = require('express');
var lessMiddleware = require('less-middleware');

var app = module.exports = express();

app.set('views', 'content');
app.set('view engine', 'jade');

// static content
app.use('/bootstrap', express.static('bower_components/bootstrap/dist/css'));
app.use('/bootstrap', express.static('bower_components/bootstrap/dist/fonts'));
app.use('/bootstrap', express.static('bower_components/bootstrap/dist/js'));
app.use('/d3', express.static('bower_components/d3'));
app.use('/jquery', express.static('bower_components/jquery/dist'));
app.use('/ubuntu', express.static('bower_components/ubuntu-fontface'));

app.use(express.static('content'));

// styles
app.use(lessMiddleware('content', {
	dest: 'content',
	force: true,
	compiler: { compress: false }
}));

// routes
app.use(require('./pages/router'));
app.use('/api', require('./api/router'));